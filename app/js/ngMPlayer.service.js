(
    function(){
        angular
        .module('ngMPlayerApp')
        .factory('ngMPlayerFactory', ngMPlayerFactory);

        ngMPlayerFactory.$inject = ['$http'];

        function ngMPlayerFactory($http)
        {
            return {
                get : function(){
                    return $http.get('/stations')
                    .then(successCallback)
                    .catch(errorCallback)
                },
                getFMStation : function(){
                  return $http.get('/fm-station')
                  .then(successCallback)
                  .catch(errorCallback)
                }
            }

            function successCallback(response) {
                return response.data;
            }

            function errorCallback(response) {

            }
        }
    }
)();
