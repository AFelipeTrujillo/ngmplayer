(
    function(){
        angular
        .module('ngMPlayerApp')
        .controller('ngMPlayerController',ngMPlayerController)

        ngMPlayerController.$inject = ['ngMPlayerFactory'];

        function ngMPlayerController(ngMPlayerFactory){
            var vm = this;
            vm.socket = null;
            vm.stations = [];
            vm.fmStation = null;
            vm.volume = 50;
            vm.setVolume = _setVolume_;
            vm.selected = [];
            vm.onSelect = _onSelect_
            vm.stop = _onDeselect_
            vm.mute = _mute_
            vm.isMute = true;
            vm.currentStationIndex = 0;
            vm.switch = true;
            init();
            return vm;

            function _setVolume_() {
                console.info('CALL volume(' + vm.volume + ')');
                vm.socket.emit('volume',{'volume' : vm.volume});
            }

            function _onSelect_(station,index){
                if(vm.stations[vm.currentStationIndex].isPlay){
                    vm.stations[vm.currentStationIndex].isPlay = false;
                }
                vm.stations[index].isPlay = true;
                vm.currentStationIndex = index;
                console.info('CALL play()');
                vm.socket.emit('play',{
                    'url' : station.url,
                    'volume' : vm.volume,
                    'id' : station.id});
            }

            function _onDeselect_(){
                console.info('CALL stop()');
                vm.socket.emit('stop');
                ngMPlayerFactory.get().then(function(data){
                    vm.stations = data;
                });
            }

            function _mute_(isMute){
                vm.isMute = isMute;
                console.log('CALL mute()');
                vm.socket.emit('mute');
            }

            function init(){
                var hostname = document.location.href;
                vm.socket = io.connect(hostname);
                ngMPlayerFactory.get().then(function(data){
                    vm.stations = data;
                });

                ngMPlayerFactory.getFMStation().then(function(data){
                    vm.fmStation = data.FM_STATION;
                });
            }
        }
    }
)();
