var express = require('express'),
    app = express()
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    path = require('path'),
    Mplayer = require('node-mplayer'),
    low = require('lowdb'),
    db = low('stations.json'),
    colors = require('colors');

db.defaults({stations : []}).value();
var stations = db.get('stations');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
  next();
});

app.use(morgan('dev'));

app.use(express.static(__dirname + '/app'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/stations', function(req, res) {
  var post = stations.value();
  res.send(post);
});

app.get('/fm-station', function(req, res) {
  res.json({"FM_STATION" : FM_STATION});
});

const port = 3000;
const FM_STATION = '88.5';

var server = app.listen(port, () => {
  console.log('PIRadio.FM is running on ' + port);
});

var io = require('socket.io')(server);

var player = null;
io.on('connection',(socket) => {
  console.log('an user connected'.blue);
  if(player !== null && player.checkPlaying() === true){
    console.log('Player is running'.yellow);
  }
  //play();
  socket.on('play',(data) => {
    try {
      if(player !== null && player.checkPlaying() === true){
        player.stop();
        db.get('stations')
        .find({ isPlay: true })
        .assign({ isPlay: false})
        .value();
      }
      player = new Mplayer(data.url);
      player.play({volume : data.volume});
      db.get('stations')
      .find({ id: data.id })
      .assign({ isPlay: true})
      .value();
      console.log("play()".yellow, data);
    } catch (error) {
      console.log('Error ',error);
    }
  });
  //stop()
  socket.on('stop',() =>{
    console.log("stop()".red);
    if(player !== null && player.checkPlaying() === true){
      player.stop();
      db.get('stations')
        .find({ isPlay: true })
        .assign({ isPlay: false})
        .value();
    }
  });
  //volume()
  socket.on('volume',function(data){
    console.log("volume()".yellow,data);
    if(player !== null && player.checkPlaying() === true){
        player.setVolume(data.volume);
    }
  });
  //mute
  socket.on('mute',function(){
    console.log("mute()".red);
    if(player !== null && player.checkPlaying() === true){
      player.mute();
    }
  });

  socket.on('disconnect', () => {
    console.log("User died");
  });
});
